var $docsify = {
    el: '#app',
    homepage: 'index.md',
    loadSidebar: true,
    autoHeader: true,
    subMaxLevel: 2,
    search: {
        maxAge: 86400000,
        placeholder: '键入以搜索',
        noData: '无结果'
    },
    alias: {
        '.*/_sidebar.md': '/posts/list.md',
        '(.*)/$': '$1/index.md',
    },
    plugins: [
        (hook) => {
            hook.doneEach(() => {
                var imgs = document.getElementsByTagName('img');
                for (let i = 0; i < imgs.length; i++) {
                    console.log(imgs[i]);
                    if (imgs[i].alt != '') {
                        insertPara(imgs[i], imgs[i].alt);
                    }
                }
                renderMathInElement(
                    document.getElementById('main'),
                    {
                        delimiters: [
                            { left: "$$", right: "$$", display: true },
                            { left: "$", right: "$", display: false }
                        ]
                    }
                );
            });
        }
    ]
}

function insertPara(e, text) {
    var p = document.createElement('span');
    p.innerText = text;
    e.after(p);
}