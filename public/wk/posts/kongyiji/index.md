# 孔乙己

数学组一开考，所有考试的人便都看着他笑。

有的叫道，“数学组，你又偷别校的试卷来考试了！”

他不回答，对考生说，“赶快做题，待会上传智学网。”便拿出一份试卷。

他们又故意的高声嚷道，“你一定偷了重庆八中的试卷了！”

数学组睁大眼睛说，“你怎么这样凭空污人清白……”

“什么清白？我前天亲眼见你把重庆八中的名字涂掉，改成什么2020届线上统测。”

数学组便涨红了脸，手上的红笔呼呼直转，争辩道，“改名不能算偷……改名！……都是西南名校联盟，能算偷么？”

接连便是难懂的话，什么“武昌糯米白”，什么“农民熟练”之类，引得众人都哄笑起来：考场内外充满了快活的空气。

![数学组发的原卷](1.png)

![去除遮罩后](2.png)
