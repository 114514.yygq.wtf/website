# CCNU Test English April 2019, extra questions

## Word Bank(partial)

### B

- `wisecrack` v.说俏皮话 (P1L1)
- `trudge` v. 跋涉 (P4L3)

### C

- $\Delta$`celiac disease` n. 乳糜泻 (P1L1)
- `disorder` v.&n. 紊乱 (P1L2)
- `molecule` n. 分子 (P2L4)
- `anti-`
  - `antibody` n. 抗体 (P3L2)
  - `antigen` n. 抗原 (P3L3)
  - ~~`anti-human` n. 钉钉~~
- `precisely` adv. 恰好 (P4L2)

### D

- `demonstration` n. 示范 (P2L1)
- `orbit` n. 轨道 (P3L1)
- `debris` n. 碎屑 (P6L2)

## Q&A

### C

- 30(`B`): `A`：将药物与身体做比较；`B`：描述化学过程。根据原文3、4段，讲到了身体中的化学过程，但并没有“将药物与身体做比较”，所以`B`恰当。

### D

- 35(`D`): 第一段"...simulating a technique that could one day collect spaceborne garbage"，卫星通过携带网来收集垃圾，而"remove debris"的也是通过"collect waste"来实现的 。

### Cloze

- 51(`B`): `A`：我个人理解这里的higher用的是引申义，可以理解为“更深入”，而better没有这层~~内味儿~~意思。
- 53(`C`): **留给群管理员讲吧^_^。**~~（我也错了，我觉得我不是人，而是䈮篛）~~
- 60(`D`): 句意：也许这并不仅仅是一瞬的直觉或同感，而是人类大爱的反映。`ABC`带入句意不通而`D`就很好。

### Fillin

- 66(`led`): （我**尝试**~~口胡~~解释一下）如果把"in the space of two weeks"删除，我认为填`had led`是可以的，可以理解为长年累月的气候变化逐渐导致冰川逐渐消融，强调过程与影响，指二月这个“过去”的之前一大段时间内的累积效应。但是原句中，冰川消融是"in the space of two weeks"，在很短的时间内发生的，或者说这些都是在二月这个“过去”发生的，并且没有涉及到影响，所以还是过去式`led`更为恰当。
- 69(`that`): 引用解析：先行词为communities且被"many"修饰，所以用`that`。

Guo Rongwei, from Jogger Magic Center

17th Febrary, 2020